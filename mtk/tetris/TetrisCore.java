package mtk.tetris;

import mtk.tetris.entities.Block;
import mtk.tetris.entities.Shape;
import org.jsfml.graphics.RenderTarget;
import org.jsfml.window.Keyboard;

import java.util.ArrayList;

public class TetrisCore {


    final int sizeX = 32;
    final int sizeY = 32;
    final int windowWidth;
    final int windowHeight;
    final float blockSize;
    public ArrayList<Shape> shapes;

    Shape activeShape;
    private final float cooldown = 1.0f;

    private float speedModifier = 10;
    private float timePassed = 0;


    public TetrisCore(int windowWidth, int windowHeight) {
        this.windowWidth = windowWidth;
        this.windowHeight = windowHeight;

        blockSize = (float) windowWidth / sizeX;

        shapes = new ArrayList<Shape>();

        Block.setBlockSize(blockSize);
        Block.setWindowHeight(windowHeight);
    }

    public void update(float dt) {
        if (timePassed < cooldown) {
            timePassed += dt * speedModifier;
        } else {
            timePassed = 0;
            if (activeShape != null) {
                activeShape.update(shapes);
            }
        }
    }
    public void handleKeyPress(){
        if (Keyboard.isKeyPressed(Keyboard.Key.D)) {
            moveActiveShape(Shape.MoveDirections.Right);
        }
        if (Keyboard.isKeyPressed(Keyboard.Key.A)) {
            moveActiveShape(Shape.MoveDirections.Left);
        }
        if (Keyboard.isKeyPressed(Keyboard.Key.S)) {
            forceUpdate();
        }
        if (Keyboard.isKeyPressed(Keyboard.Key.Q)) {
            makeNewShape();
        }
        if (Keyboard.isKeyPressed(Keyboard.Key.E)) {
            rotateActiveShape();
        }
    }
    public void renderOn(RenderTarget t){
        for (Shape s : shapes) {
            for (Block b : s.blocks) {
                t.draw(b);
            }
        }
    }
    public void forceUpdate() {
        if (activeShape != null) {
            activeShape.update(shapes);
        }
    }

    void makeNewShape() {
        clearLines();
        activeShape = new Shape(blockSize, windowWidth);
        shapes.add(activeShape);
    }

    void rotateActiveShape() {
        activeShape.rotate();
    }

    void moveActiveShape(Shape.MoveDirections direction) {
        activeShape.moveHorizontally(shapes, direction);
    }

    void clearLines() {
        int[] rowCounters;
        boolean done = false;
        //Remove lines
        while (!done) {
            rowCounters = new int[sizeY];
            done = true;
            for (Shape s : shapes) {
                for (Block b : s.blocks) {
                    int index = (int) (b.getPosition().y / blockSize);
                    rowCounters[index]++;
                }
            }
            for (int i = 0; i < rowCounters.length; i++) {
                if (rowCounters[i] == sizeX) {
                    done = false;
                    for (Shape s : shapes) {
                        ArrayList<Block> blocksToRemove = new ArrayList<>(s.blocks.size());
                        for (Block b : s.blocks) {
                            int index = (int) (b.getPosition().y / blockSize);
                            if (index == i) {
                                blocksToRemove.add(b);
                            }
                        }
                        if (!blocksToRemove.isEmpty()) {
                            for (Block b : blocksToRemove) {
                                s.blocks.remove(b);
                            }
                        }
                    }
                }
            }
        }
        // move hanging shapes
        if (!shapes.isEmpty()) {
            done = false;
            while (!done) {
                for (Shape s : shapes) {
                    if (!s.update(shapes)) {
                        done = true;
                    }
                }
            }
        }
    }
}

