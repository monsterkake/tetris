package mtk.tetris.entities;

import org.jsfml.graphics.RectangleShape;
import org.jsfml.system.Vector2f;

import java.util.ArrayList;

public class Block extends RectangleShape {
    public static float blockSize;
    private static int windowHeight;

    public Block(float x, float y) {
        super(new Vector2f(blockSize, blockSize));
        setPosition(x, y);
    }

    public static void setBlockSize(float blockSize) {
        Block.blockSize = blockSize;
    }

    public static void setWindowHeight(int windowHeight) {
        Block.windowHeight = windowHeight;
    }

    public boolean isInVerticalWindowBounds() {
        return (getPosition().y + blockSize) < windowHeight;
    }

    boolean collidesVerticallyWith(ArrayList<Block> blocks) {
        int futurePos = (int) (this.getPosition().y + blockSize);

        for (Block b : blocks) {
            if (futurePos == (int) b.getPosition().y && ((int) b.getPosition().x == (int) this.getPosition().x)) {
                return true;
            }
        }
        return false;
    }

    boolean collidesHorizontallyWith(ArrayList<Block> blocks, Shape.MoveDirections direction) {
        float dx = Block.blockSize * direction.value;
        int futurePos = (int) (this.getPosition().x + dx);

        for (Block b : blocks) {
            if (futurePos == (int) b.getPosition().x && ((int) b.getPosition().y == (int) this.getPosition().y)) {
                return true;
            }
        }
        return false;
    }

    public boolean isInHorizontalWindowBounds(Shape.MoveDirections direction) {
        float dx = Block.blockSize * direction.value;
        return 0 <= (getPosition().x + dx) && (getPosition().x + dx) < windowHeight;
    }
}
