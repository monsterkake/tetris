package mtk.tetris.entities;

import org.jsfml.graphics.Color;
import org.jsfml.system.Vector2i;


import java.util.ArrayList;
import java.util.Random;

public class Shape {
    public enum MoveDirections {
        Left(-1),
        Right(1);
        public final float value;

        MoveDirections(float value) {
            this.value = value;
        }
    }

    public ArrayList<Block> blocks;

    public Shape(float blockSize, int windowWidth) {
        blocks = new ArrayList<Block>(4);
        Random rand = new Random();
        int boundX = (int) (windowWidth / blockSize);
        int currX = boundX / 2;
        int currY = 0;
        Block first = new Block(currX * blockSize, currY * blockSize);
        blocks.add(first);

        int[] moveVector = {0, 0};
        for (int i = 0; i < 3; i++) {
            int axis = rand.nextInt(2);
            moveVector[1 - axis] = 0;

            int dx = 0;
            while (dx == 0 || dx == -moveVector[axis]) {
                dx = rand.nextInt(3) - 1;
            }
            moveVector[axis] = dx;

            currX += moveVector[0];
            currY += moveVector[1];

            blocks.add(new Block(currX * blockSize, currY * blockSize));
        }

        org.jsfml.graphics.Color randomColor = new org.jsfml.graphics.Color(rand.nextInt(255), rand.nextInt(255), rand.nextInt(255), 255);
        for (Block b : blocks) {
            b.setFillColor(randomColor);
            b.setOutlineColor(Color.BLACK);
            b.setOutlineThickness(1);
        }
    }

    public boolean update(ArrayList<Shape> shapes) {
        for (Block b : blocks) {
            if (!b.isInVerticalWindowBounds()) {
                return false;
            }
            for (Shape s : shapes) {
                if (this != s) {
                    if (b.collidesVerticallyWith(s.blocks)) {
                        return false;
                    }
                }
            }
        }
        for (Block b : blocks) {
            b.move(0, Block.blockSize);
        }
        return true;
    }

    public void moveHorizontally(ArrayList<Shape> shapes, MoveDirections direction) {
        for (Block b : blocks) {
            if (!b.isInHorizontalWindowBounds(direction)) {
                return;
            }
            for (Shape s : shapes) {
                if (this != s) {
                    if (b.collidesHorizontallyWith(s.blocks, direction)) {
                        return;
                    }
                }
            }
        }
        for (Block b : blocks) {
            float dx = Block.blockSize * direction.value;
            b.move(dx, 0);
        }
    }

    public void rotate() {
        int size = blocks.size();
        Vector2i[] shiftedPoints = new Vector2i[size];
        Vector2i[] shiftedAndRotatedPoints = new Vector2i[size];
        Vector2i[] rotatedPoints = new Vector2i[size];

        Vector2i referencePoint = new Vector2i(
                (int) (blocks.get(0).getPosition().x / Block.blockSize),
                (int) (blocks.get(0).getPosition().y / Block.blockSize));
        final int[][] rotationMatrix = {
                {0, -1},
                {1, 0}
        };
        for (int i = 0; i < size; i++) {
            int x = (int) (blocks.get(i).getPosition().x / Block.blockSize);
            int y = (int) (blocks.get(i).getPosition().y / Block.blockSize);

            // Shift to origin
            shiftedPoints[i] = new Vector2i(
                    x - referencePoint.x,
                    y - referencePoint.y
            );

            // Rotate
            shiftedAndRotatedPoints[i] = new Vector2i(
                    shiftedPoints[i].x * rotationMatrix[0][0] + shiftedPoints[i].y * rotationMatrix[0][1],
                    shiftedPoints[i].x * rotationMatrix[1][0] + shiftedPoints[i].y * rotationMatrix[1][1]
            );

            // Back to initial reference
            rotatedPoints[i] = new Vector2i(
                    shiftedAndRotatedPoints[i].x + referencePoint.x,
                    shiftedAndRotatedPoints[i].y + referencePoint.y
            );
            // apply rotation to actual blocks
            blocks.get(i).setPosition(
                    rotatedPoints[i].x * Block.blockSize,
                    rotatedPoints[i].y * Block.blockSize
            );
        }
    }
}
