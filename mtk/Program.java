package mtk;

import mtk.tetris.TetrisCore;
import org.jsfml.graphics.RenderTexture;
import org.jsfml.graphics.RenderWindow;
import org.jsfml.graphics.Sprite;
import org.jsfml.graphics.TextureCreationException;
import org.jsfml.window.Keyboard;
import org.jsfml.window.VideoMode;
import org.jsfml.window.event.Event;
import org.jsfml.system.Clock;

public class Program {
    RenderWindow window;

    RenderTexture renderTexture;

    TetrisCore tetrisCore;
    TetrisCore tetrisCore2;

    public void loop() {
        Clock clock = new Clock();
        float dt;
        clock.restart();

        while (window.isOpen()) {
            dt = clock.getElapsedTime().asSeconds();
            clock.restart();
            input();
            update(dt);
            render();
        }
    }

    public void init() {
        final int width = 500;
        final int height = 500;

        window = new RenderWindow(new VideoMode(width*2, height, 16), "main");

        renderTexture = new RenderTexture();
        try {
            renderTexture.create(width,height);
        } catch (TextureCreationException e) {
            throw new RuntimeException(e);
        }
        tetrisCore = new TetrisCore(width, height);
        tetrisCore2 = new TetrisCore(width, height);
    }

    public void update(float dt) {
        tetrisCore.update(dt);
        tetrisCore2.update(dt);
    }

    public void render() {
        window.clear();

        renderTexture.clear();
        tetrisCore.renderOn(renderTexture);
        renderTexture.display();
        Sprite sprite = new Sprite(renderTexture.getTexture());
        window.draw(sprite);

        renderTexture.clear();
        tetrisCore2.renderOn(renderTexture);
        renderTexture.display();
        sprite.setTexture(renderTexture.getTexture());
        sprite.setPosition(500,0);
        window.draw(sprite);

        window.display();
    }

    public void input() {
        for (Event e : window.pollEvents()) {
            switch (e.type) {
                case CLOSED -> {
                    window.close();
                }
                case KEY_PRESSED -> {
                    if (Keyboard.isKeyPressed(Keyboard.Key.ESCAPE)) {
                        window.close();
                        break;
                    }
                    tetrisCore.handleKeyPress();
                    tetrisCore2.handleKeyPress();
                }
            }
        }
    }
}
